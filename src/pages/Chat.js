import React from 'react';

const apiMock = () => {
    // set our local storage, usually it comes from your server
    if(!localStorage.getItem('qismo-widget')) localStorage.setItem('qismo-widget', JSON.stringify({"user_id":"fikri@qiscus.com","user_name":"Fikri"}));
    // initialize our qismo
    new window.Qismo('ysotr-aja9u91emy3ecss');
    window.setTimeout(() => {
      console.log('API called', window.Qismo);
    }, 3000);
}

export default class Chat extends React.Component {
    componentDidMount() {
        var s,t; s = document.createElement('script'); s.type = 'text/javascript';
        s.id = 'qismo-script';
        // s.src = 'https://s3-ap-southeast-1.amazonaws.com/qiscus-sdk/public/qismo/qismo-v2.js'; s.async = true;
        s.src = '/qismo-v2.js'; s.async = true;
        s.onload = s.onreadystatechange = apiMock;
        t = document.getElementsByTagName('script')[0]; t.parentNode.insertBefore(s, t);
    }
    componentWillUnmount() {
        document.getElementById('qismo-script') && document.getElementById('qismo-script').remove();
        document.querySelector('.qcw-container') && document.querySelector('.qcw-container').remove();
        document.querySelector('.qcw-cs-container') && document.querySelector('.qcw-cs-container').remove();
        document.querySelector('.qismo-extra') && document.querySelector('.qismo-extra').remove();
    }
    render() {
        return <div><h1>Chat</h1></div>
    }
}